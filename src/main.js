import Vue from 'vue';
import VueHeadful from 'vue-headful';
import VueLodash from 'vue-lodash';
import axios from 'axios';
import lodash from 'lodash';
import App from './App.vue';
import router from './router';
import store from './store/index';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en';
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';
import '@/styles/index.scss';

// Use Plugins
Vue.use(ElementUI, { locale });
Vue.use(VueLodash, { lodash: lodash });

Vue.component('vue-headful', VueHeadful);

Vue.config.productionTip = false;

// Global axios defaults
axios.defaults.baseURL = process.env.VUE_APP_API_URL;
axios.defaults.headers.common = {
	'X-API-Key': process.env.VUE_APP_API_KEY,
};

new Vue({
	render: h => h(App),
	router,
	store,
	components: { App },
}).$mount('#app');