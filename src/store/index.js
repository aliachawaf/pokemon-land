import Vue from 'vue';
import Vuex from 'vuex';
import fixedData from './modules/fixedData';
import catalog from './modules/catalog';
import shoppingcart from './modules/shoppingcart';

Vue.use(Vuex);

export default new Vuex.Store({
	// In strict mode, whenever Vuex state is mutated outside of mutation handlers,
	// an error will be thrown. This ensures that all state mutations can be explicitly
	// tracked by debugging tools.
	strict: true,
	modules: {
		fixedData,
		catalog,
		shoppingcart,
	},
});
