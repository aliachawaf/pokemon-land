// Module used to handle user shopping cart
import Vue from 'vue';

const state = {
	shoppingCartCards: [
		// { card, price, quantity }
	],
};

const getters = {
	shoppingCartCards: () => state.shoppingCartCards,
	shoppingCartTotalItems: () => Vue._.sumBy(state.shoppingCartCards, 'quantity'),
	shoppingCartTotalAmount: () => {
		return Vue._
			.chain(state.shoppingCartCards)
			.map(item => item.price*item.quantity)
			.sum()
			.value()
			.toFixed(2);
	},
};

const actions = {
};

const mutations = {
	CLEAR_SHOPPING_CART_CARDS(state) {
		state.shoppingCartCards = [];
	},
	ADD_CARD_IN_SHOPPING_CART(state, { card, price, quantity }) {
		const index = Vue._.findIndex(state.shoppingCartCards, item => item.card.id === card.id);

		if (index === -1) state.shoppingCartCards.push({ card, price, quantity });
		else {
			const newQuantity = quantity + state.shoppingCartCards[index].quantity;

			state.shoppingCartCards = Vue._
				.chain(state.shoppingCartCards)
				.cloneDeep()
				.set(index, { card, price, quantity: newQuantity })
				.value();
		}
	},
	REMOVE_CARD_FROM_SHOPPING_CART(state, cardId) {
		Vue._.remove(state.shoppingCartCards, item => item.card.id === cardId);
	},
	SET_CARD_QUANTITY(state, { cardId, newQuantity }) {
		const index = Vue._.findIndex(state.shoppingCartCards, item => item.card.id === cardId);
		state.shoppingCartCards[index].quantity = newQuantity;
	},
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations,
};
