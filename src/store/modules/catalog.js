// Module used to handle Pokemon Land catalog (Pokemon cards, filters ...)

import Vue from 'vue';
import axios from 'axios';

const CancelToken = axios.CancelToken;

const defaultFilters = {
	'set.id': [],
	types: [],
	subtypes: [],
	supertype: [],
	rarity: [],
};

const state = {
	apiUrl: '/cards',
	cards: {},
	cardsPaginationInfos: {
		currentPage: 1,
		pageSize: 20,
		totalCards: 0,
	},
	cardsSearchInfos: {
		searchText: '',
		filters: { ...defaultFilters },
		searchMode: 0, // Used for search operator (0 for OR, 1 for AND)
	},
	cardsOrderInfos: {
		orderKey: 'name',
		orderName: 'Name',
		orderMode: 'ascending', // or can be 'descending'
	},
	favoriteCards: [],
	lastSeenCards: []
};

const getters = {
	// Cards
	pokemonCards: () => state.cards,
	pokemonCard: () => (id) => Vue._.find(state.cards, ['id', id]),
	userFavoriteCards: () => state.favoriteCards,
	lastSeenCards: () => state.lastSeenCards,
	// Pagination
	currentPagePokemonCards: () => state.cardsPaginationInfos.currentPage,
	pageSizePokemonCards: () => state.cardsPaginationInfos.pageSize,
	totalPokemonCards: () => state.cardsPaginationInfos.totalCards,
	// Filters
	pokemonCardsTextSearch: () => state.cardsSearchInfos.searchText,
	pokemonCardsFilters: () => state.cardsSearchInfos.filters,
	pokemonCardsSearchMode: () => state.cardsSearchInfos.searchMode,
	// Sorting
	pokemonCardsOrder: () => state.cardsOrderInfos,
};

const actions = {
	fetchPokemonCards({ commit }) {
		// Used for search text (enable to cancel previous api call while writing in text search input)
		if (this.source) this.source.cancel();
		this.source = CancelToken.source();

		// Search text on Pokemon card name
		let query = `name:*${state.cardsSearchInfos.searchText}*`;

		// Format query search filters following API syntax (ex: "q=supertype:pokemon types:dragon")
		const formattedFilters = Vue._
			.chain(state.cardsSearchInfos.filters)
			.map((filterValues, filterName) => filterValues.map(value => filterName+':"'+value+'"'))
			.flatten()
			.value();

		if (!Vue._.isEmpty(formattedFilters)) {
			if (state.cardsSearchInfos.searchMode === 0) query += ` (${formattedFilters.join(' OR ')})`;
			else query += ` (${formattedFilters.join(' AND ')})`;
		}

		// Format pagination arguments
		const pagination = `page=${state.cardsPaginationInfos.currentPage}&pageSize=${state.cardsPaginationInfos.pageSize}`;

		// Assemble final url
		let url = `${state.apiUrl}?${pagination}`;
		if (query) url += `&q=${query}`;

		url += '&orderBy=';
		if (state.cardsOrderInfos.orderMode === 'descending') url += '-';
		url += state.cardsOrderInfos.orderKey;

		return axios
			.get(url, { cancelToken: this.source.token })
			.then((response) => {
				commit('SET_CARDS', response.data.data);

				// Set card pagination infos
				commit('SET_CURRENT_PAGE_CARDS', response.data.page);
				commit('SET_PAGE_SIZE_CARDS', response.data.pageSize);
				commit('SET_TOTAL_CARDS', response.data.totalCount);

				return Promise.resolve();
			})
			.catch((err) => {
				if (axios.isCancel(err)) return;
				return Promise.reject(`API error: ${err}`);
			});
	},
};

const mutations = {
	SET_CARDS(state, cards) {
		state.cards = cards;
	},
	// Pagination setters
	SET_CURRENT_PAGE_CARDS(state, page) {
		state.cardsPaginationInfos.currentPage = page;
	},
	SET_PAGE_SIZE_CARDS(state, pageSize) {
		state.cardsPaginationInfos.pageSize = pageSize;
	},
	SET_TOTAL_CARDS(state, total) {
		state.cardsPaginationInfos.totalCards = total;
	},
	// Filters
	SET_CARDS_FILTER(state, { filterName, value }) {
		state.cardsSearchInfos.filters[filterName] = value;
	},
	CLEAR_CARDS_FILTERS(state) {
		state.cardsSearchInfos.filters = { ...defaultFilters };
	},
	SET_CARDS_SEARCH_MODE(state, value) {
		state.cardsSearchInfos.searchMode = value;
	},
	SET_CARDS_SEARCH_TEXT(state, value) {
		state.cardsSearchInfos.searchText = value;
	},
	// Sorting
	SET_CARDS_ORDER(state, { orderKey, orderName, orderMode }) {
		state.cardsOrderInfos.orderKey = orderKey;
		state.cardsOrderInfos.orderName = orderName;
		state.cardsOrderInfos.orderMode = orderMode;
	},
	// Favorites
	ADD_FAVORITE_CARD(state, cardId) {
		state.favoriteCards.push(cardId);
	},
	REMOVE_FAVORITE(state, cardId) {
		state.favoriteCards = Vue._.reject(state.favoriteCards, id => id === cardId);
	},
	// Last seen
	ADD_LAST_SEEN_CARD(state, card) {
		if (!state.lastSeenCards.includes(card)) state.lastSeenCards.unshift(card);
		if (state.lastSeenCards.length > 5) state.lastSeenCards.pop();
	},
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations,
};
