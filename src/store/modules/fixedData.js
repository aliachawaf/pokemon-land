// Module used to handle all Pokemon fixed data such as sets, types, subtypes ...

import Vue from 'vue';
import axios from 'axios';

const state = {
	fixedDataWasLoaded: false,
	fixedDataInfos: {
		sets: {
			// Fixme: bas url should be in axios default config
			url: '/sets',
			setter: 'SET_SETS',
			values: [],
		},
		types: {
			url: '/types',
			setter: 'SET_TYPES',
			values: [],
		},
		subtypes: {
			url: '/subtypes',
			setter: 'SET_SUBTYPES',
			values: [],
		},
		supertypes: {
			url: '/supertypes',
			setter: 'SET_SUPERTYPES',
			values: [],
		},
		rarities: {
			url: '/rarities',
			setter: 'SET_RARITIES',
			values: [],
		},
	},
};

const getters = {
	pokemonSets: () => state.fixedDataInfos.sets.values,
	pokemonTypes: () => state.fixedDataInfos.types.values,
	pokemonSubtypes: () => state.fixedDataInfos.subtypes.values,
	pokemonSupertypes: () => state.fixedDataInfos.supertypes.values,
	pokemonRarities: () => state.fixedDataInfos.rarities.values,
};

const actions = {
	fetchAllFixedData({ state, dispatch, commit }) {
		// Do not fetch data if already done before
		if (state.fixedDataWasLoaded) return Promise.resolve();

		// Else, fetch all fixed pokemon data
		const promises = Vue._.map(state.fixedDataInfos, (dataInfos) => dispatch('fetchFixedData', dataInfos));

		return Promise
			.all(promises)
			.then(() => {
				commit('SET_FIXED_DATA_LOADED');
				return Promise.resolve();
			})
			.catch(err => Promise.reject(err));
	},
	fetchFixedData({ commit }, dataInfos) {
		return axios
			.get(dataInfos.url)
			.then((response) => {
				commit(dataInfos.setter, response.data.data);
				return Promise.resolve();
			});
	},
};

const mutations = {
	SET_FIXED_DATA_LOADED(state) {
		state.fixedDataWasLoaded = true;
	},
	SET_SETS(state, elements) {
		state.fixedDataInfos.sets.values = elements;
	},
	SET_TYPES(state, elements) {
		state.fixedDataInfos.types.values = elements;
	},
	SET_SUBTYPES(state, elements) {
		state.fixedDataInfos.subtypes.values = elements;
	},
	SET_SUPERTYPES(state, elements) {
		state.fixedDataInfos.supertypes.values = elements;
	},
	SET_RARITIES(state, elements) {
		state.fixedDataInfos.rarities.values = elements;
	},
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations,
};
