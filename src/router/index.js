import Vue from 'vue';
import Router from 'vue-router';
import Catalog from '@/views/Catalog';
import ShoppingCart from '@/views/ShoppingCart';
import Favorites from '@/views/Favorites';
import PageNotFound from '@/views/PageNotFound';
import store from '@/store';

Vue.use(Router);

// Create the route constant for use into web application, each route is defined by:
// path : string, route path to load the route
// name : string, name to define the router
// component: Component, associated component to load for this route
// meta : {title, index}, meta information of the route.
//        The 'title' is used for breadcrumb component,
//        and 'index' is used for sidebar component.
export const constantRouterMap = [
	{
		path: '/catalog',
		name: 'Catalog',
		component: Catalog,
		meta: {
			breadcrumbName: 'Pokemon catalog',
		}
	},
	{
		path: '/',
		redirect: '/catalog'
	},
	{
		path: '/shopping-cart',
		name: 'ShoppingCart',
		component: ShoppingCart,
		meta: {
			breadcrumbName: 'Shopping cart',
		}
	},
	{
		path: '/favorites',
		name: 'Favorites',
		component: Favorites,
		meta: {
			breadcrumbName: 'Your favorites Pokemon cards',
		}
	},
	{
		path: '/not-found',
		name: 'PageNotFound',
		components: { default: PageNotFound },
	},
	{
		path: '*',
		redirect: '/not-found'
	}
];

const router = new Router({
	mode: 'history',
	routes: constantRouterMap,
});

// Todo: handle user authentication and error callbacks and sessions in router.beforeEach function
router.beforeEach((to, from, next) => {
	// Fetch and store Pokemon fixed data (sets, types ...)
	store.dispatch('fixedData/fetchAllFixedData')
		.then(() => next())
		.catch(() => next(false));
});

export default router;
