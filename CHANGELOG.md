# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] - 2021-xx-xx

### Added
- Catalog template (with search form, cards, pagination, last seen cards)
- Special offer alert (above Header)
- Header template

### Changed
### Fixed
### Removed
